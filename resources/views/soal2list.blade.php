@extends('layouts.mainlayout')
@section('content')
    <div class="album text-muted">
        <div class="container">
            <a href="{{route("soal2")}}">Tambah Pegawai</a>
            <table class="table">
                <tr>
                    <th>Nama Pegawai</th>
                    <th>Umur</th>
                    <th>Action</th>
                </tr>
                <tbody>
                @foreach($list as $r)
                <tr>
                    <td>{{$r->pegawai_nama}}</td>
                    <td>{{$r->pegawai_umur}}</td>
                    <td>
                        <a href="{{route("soal2edit",['pegawai_id'=>$r->pegawai_id])}}">Edit</a>
                        <a href="{{route("soal2delete",['pegawai_id'=>$r->pegawai_id])}}">Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        // $( "#soal1" ).on( "submit", function( event ) {
        //     var check = "";
        //     $('.hobi:checkbox:checked').each(function () {
        //          check += (this.checked ? $(this).val()+", " : "");
        //     });
        //
        //     alert( "Nama Lengkap: "+$("#fullname").val()+
        //         "\nTempat Tanggal Lahir: "+$("#tempat").val()+","+$("#tanggal").val()+"/"+$("#bulan").val()+"/"+$("#tahun").val()+
        //         "\nAlamat: "+$("#alamat").val()+
        //         "\nNo. Telp/Hp: "+$("#nohp").val()+
        //         "\nJenis Kelamin: "+$("#jeniskelamin").val()+
        //         "\nAgama: "+$("#agama").val()+
        //         "\nHobi: "+check
        //     );
        //     event.preventDefault();
        // });
        // $("#tampil").click(function () {
        //     alert("aa");
        // });
    </script>
@endsection

