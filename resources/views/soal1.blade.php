@extends('layouts.mainlayout')
@section('content')
    <div class="album text-muted">
        <div class="container">
            <form id="soal1">
                <div class="row">
                    <div class="col-12 ">
                        <div class="card">
                            <div class="card-header text-center"> Form Data Diri</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <label>Nama Lengkap</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" id="fullname">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Tempat, Tanggal Lahir</label>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-3">
                                                <input type="text" class="form-control" id="tempat" placeholder="tempat">
                                            </div>
                                            <div class="col-3">
                                                <input type="number" class="form-control" id="tanggal" placeholder="tanggal">
                                            </div>
                                            <div class="col-3">
                                                <input type="number" class="col-3 form-control" id="bulan" placeholder="bulan">
                                            </div>
                                            <div class="col-3">
                                                <input type="number" class="col-3 form-control" id="tahun" placeholder="tahun">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Alamat</label>
                                    </div>
                                    <div class="col">
                                        <textarea class="form-control" id="alamat"> </textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>No. Telp Hp</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" id="nohp" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>Jenis Kelamin</label>
                                    </div>
                                    <div class="col">
                                        <select id="jeniskelamin">
                                            @foreach($jeniskelamin as $k=>$v)
                                                <option value="{{$v}}">{{$v}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>Agama</label>
                                    </div>
                                    <div class="col">
                                        <select id="agama">
                                            @foreach($agama as $k=>$v)
                                                <option value="{{$v}}">{{$v}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label>Hobi</label>
                                    </div>
                                    <div class="col">
                                        @foreach($hobi as $k=>$v)
                                            <input type="checkbox" class="hobi" id="hobi{{$k}}" name="hobi[]" value="{{$v}}">
                                            <label for="hobi{{$k}}"> {{$v}}</label><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center ">
                                <button class="btn btn-success" id="tampil">Tampilkan</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $( "#soal1" ).on( "submit", function( event ) {
            var check = "";
            $('.hobi:checkbox:checked').each(function () {
                 check += (this.checked ? $(this).val()+", " : "");
            });

            alert( "Nama Lengkap: "+$("#fullname").val()+
                "\nTempat Tanggal Lahir: "+$("#tempat").val()+","+$("#tanggal").val()+"/"+$("#bulan").val()+"/"+$("#tahun").val()+
                "\nAlamat: "+$("#alamat").val()+
                "\nNo. Telp/Hp: "+$("#nohp").val()+
                "\nJenis Kelamin: "+$("#jeniskelamin").val()+
                "\nAgama: "+$("#agama").val()+
                "\nHobi: "+check
            );
            event.preventDefault();
        });
        // $("#tampil").click(function () {
        //     alert("aa");
        // });
    </script>
@endsection

