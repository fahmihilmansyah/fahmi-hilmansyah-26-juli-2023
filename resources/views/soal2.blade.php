@extends('layouts.mainlayout')
@section('content')
    <div class="album text-muted">
        <div class="container">
            <form id="soal2" method="post" action="{{"soal2create"}}">
                @csrf
                <div class="row">
                    <div class="col-12 ">
                        <div class="card">
                            <div class="card-header text-center"> Form Data Diri</div>
                            <input type="hidden" name="pegawai_id" value="{{!empty($items)?$items->pegawai_id:''}}">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <label>Nama Lengkap</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="pegawai_nama" class="form-control" value="{{!empty($items)?$items->pegawai_nama:''}}" id="fullname">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Jabatan</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="pegawai_jabatan" class="form-control" value="{{!empty($items)?$items->pegawai_jabatan:''}}" id="fullname">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Umur</label>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="pegawai_umur" value="{{!empty($items)?$items->pegawai_umur:''}}" class="form-control" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Alamat</label>
                                    </div>
                                    <div class="col">
                                        <textarea name="pegawai_alamat" class="form-control" id="alamat"> {{!empty($items)?$items->pegawai_alamat:''}} </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center ">
                                <button class="btn btn-success" id="tampil">Submit</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        // $( "#soal1" ).on( "submit", function( event ) {
        //     var check = "";
        //     $('.hobi:checkbox:checked').each(function () {
        //          check += (this.checked ? $(this).val()+", " : "");
        //     });
        //
        //     alert( "Nama Lengkap: "+$("#fullname").val()+
        //         "\nTempat Tanggal Lahir: "+$("#tempat").val()+","+$("#tanggal").val()+"/"+$("#bulan").val()+"/"+$("#tahun").val()+
        //         "\nAlamat: "+$("#alamat").val()+
        //         "\nNo. Telp/Hp: "+$("#nohp").val()+
        //         "\nJenis Kelamin: "+$("#jeniskelamin").val()+
        //         "\nAgama: "+$("#agama").val()+
        //         "\nHobi: "+check
        //     );
        //     event.preventDefault();
        // });
        // $("#tampil").click(function () {
        //     alert("aa");
        // });
    </script>
@endsection

