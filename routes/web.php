<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/demo', function () {
    return view('demo');
});

Route::get('/',[\App\Http\Controllers\Testpraktek::class,"soal1"])->name('soal1');
Route::get('/soal1',[\App\Http\Controllers\Testpraktek::class,"soal1"])->name('soal1');
Route::get('/soal2list',[\App\Http\Controllers\Testpraktek::class,"soal2list"])->name('soal2list');
Route::get('/soal2',[\App\Http\Controllers\Testpraktek::class,"soal2"])->name('soal2');
Route::get('/soal2edit',[\App\Http\Controllers\Testpraktek::class,"soal2"])->name('soal2edit');
Route::get('/soal2delete',[\App\Http\Controllers\Testpraktek::class,"soal2delete"])->name('soal2delete');
Route::post('/soal2create',[\App\Http\Controllers\Testpraktek::class,"soal2create"])->name('soal2create');
