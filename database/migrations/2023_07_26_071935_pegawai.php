<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('pegawai', function (Blueprint $table) {
            $table->integer("pegawai_id",true)->primary();
            $table->string('pegawai_nama',20);
            $table->string('pegawai_jabatan',20);
            $table->integer('pegawai_umur');
            $table->text('pegawai_alamat');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
