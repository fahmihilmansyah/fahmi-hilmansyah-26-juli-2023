<?php


namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Testpraktek extends Controller{
    function soal1(Request $request){
        $data=[
           "jeniskelamin"=>[
               "Laki-Laki",
               "Perempuan",
               ],
            "agama"=>[
                'islam',
                'Kristen',
                'Budha'
            ],
            'hobi'=>['Baca Buku','Olah Raga','Main Game','Hiking']
        ];
        return view("soal1",$data);
    }
    function soal2(Request $request){
        $data=[];
        $pegawai_id = $request->input('pegawai_id');
        if(!empty($pegawai_id)){
            $data = Pegawai::query()->where(['pegawai_id'=>$pegawai_id])->first();
        }
        return view("soal2",['items'=>$data]);
    }
    function soal2list(Request $request){
        $getList = DB::table('pegawai')->get()->all();
        return view("soal2list",['list'=>$getList]);
    }
    function soal2create(Request $request){
        $pegawai_id=$request->input("pegawai_id");
        $pegawai_nama=$request->input("pegawai_nama");
        $pegawai_jabata=$request->input("pegawai_jabatan");
        $pegawai_umur=$request->input("pegawai_umur");
        $pegawai_alamat=$request->input("pegawai_alamat");
        $data=[
            'pegawai_nama'=>$pegawai_nama,
            'pegawai_alamat'=>$pegawai_alamat,
            'pegawai_umur'=>$pegawai_umur,
            'pegawai_jabatan'=>$pegawai_jabata,
            ];
        if (!empty($pegawai_id)){
            DB::table('pegawai')->where(['pegawai_id'=>$pegawai_id])->update($data);
        }else{
            Pegawai::insert($data);
        }
        return redirect("soal2list");
    }
    function soal2delete(Request $request){
        $pegawai_id= $request->input("pegawai_id");
        if(!empty($pegawai_id)){
            $getList = DB::table('pegawai')->where(['pegawai_id'=>$pegawai_id])->delete();
        }
        return redirect("soal2list");
    }
}
